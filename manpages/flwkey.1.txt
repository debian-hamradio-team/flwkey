// Process this file with AsciiDoc:
// Command: a2x.py --doctype manpage --format manpage --no-xmllint flwkey.1.txt
:doctype: manpage
:man source: FLWKEY
:man manual: FLWKEY Man Page
:man date: {date}
:man version: Version 1.1.6

= flwkey(1)

== NAME

flwkey - interface to Winkeyer v1 and v2 CW code generators


== SYNOPSIS

*flwkey* - is a simple interface to the Winkeyer series of CW code generators.
It can be used with both the Winkeyer 1.x and 2.x series as well as the 
RigExpert and other units that either use the K1EL chipset or emulate it's
behavior.


== BUGS

If you find a bug or suspect *'flwkey'* is not acting as you think it should,
send an email with as much detail as possible to: <fldigi-devel@lists.sourceforge.net>


== AUTHORS
-----
Dave Freeze, W1HKJ, <w1hkj@w1hkj.com>
-----


== RESOURCES
-----
Project Site: ... <http://sourceforge.net/projects/fldigi/>
Main web site: .. <http://www.w1hkj.com/Fldigi.html>
Documentation: .. <http://www.w1hkj.com/flwkey-help/index.html>
-----


== DEBIAN MAINTAINERS
-----
Maintainer ..: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Uploaders ...: Greg Beam <ki7mt@yahoo.com>, Kamal Mostafa <kamal@whence.com>
IRC .........: OFTC #debian-hams, FREENODE: #ubuntu-hams
-----

